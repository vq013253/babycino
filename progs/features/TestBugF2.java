class TestFeatureF {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
	// This makes sure that when x || y ans = 1 which means that its returned true
	// If ans remains equal to 0 then it has returned false. 
    public int f(){
		boolean x;
		boolean y;
		int ans;
		x = true;
		y = false;
		ans = 0;
		while (x || y) {
			ans = 1;
			x = false;
		}
		return ans;
	}

}
