class TestFeatureF {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
	// This makes sure that when x != y ans = 1 which means that its returned true
	// If ans remains equal to 0 then it has returned false. 
    public int f(){
		int x;
		int y;
		int ans;
		x = 10;
		y = 10;
		if (x != y) {
			ans = 1;
		}
		else{
			ans=0;
		}
		return ans;
	}

}
