class TestFeatureF {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
	// This makes sure that when x != y && y!=z  ans = 1 which means that its returned true
	// If ans remains equal to 0 then it has returned false. 
    public int f(){
		int ans;
		int x;
		int y;
		int z;
		x = 10;
		y = 2;
		z = 3;ans=0;
		if(x != y && y != z)  {
			ans = 1;
		}
		else{
			ans=0;
		}
		return ans;
	}

}
